#!/bin/bash
mkdir -p workstation
cd workstation
git clone https://gitlab.com/lsi-ufcg/cytestion/gui-testing-study/applications/spring-petclinic.git
git clone https://gitlab.com/lsi-ufcg/cytestion/gui-testing-study/applications/spring-shop-n3xt.git
git clone https://gitlab.com/lsi-ufcg/cytestion/gui-testing-study/applications/website-bistro-restaurant.git
git clone https://gitlab.com/lsi-ufcg/cytestion/gui-testing-study/applications/website-learn-educational.git
git clone https://gitlab.com/lsi-ufcg/cytestion/gui-testing-study/applications/website-school-educational.git